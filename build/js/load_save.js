VMLS = function() {

}

VMLS.Load = function(slotID) {
  VM.Exec("load_slot "+slotID);
}

VMLS.Save = function(slotID) {
  VM.Exec("save_slot "+slotID);
}

$(function() {
  $("body").append("<div id='loadDropdown' class=\"dropDown topButton hidden\"></div>");
  $("body").append("<div id='saveDropdown' class=\"dropDown topButton hidden\"></div>");

  var ldd = WMKit.Register("loadDropdown");
  var sdd = WMKit.Register("saveDropdown");

  ldd.exclusive = true;
  sdd.exclusive = true;
  ldd.forceRegen = true;
  sdd.forceRegen = true;

  ldd.generator = function() {
    var rect = $("#btLoad")[0].getBoundingClientRect();
    $("#loadDropdown").css('left', rect.x - parseInt($("#btLoad").css('padding-left')) - 1);
    $("#loadDropdown").css('top', rect.y/* + rect.height + parseInt($("#btLoad").css('padding-bottom'))*/ - parseInt($("#btLoad").css('padding-top')));

    var ret = "<div onclick=\"WMKit.Hide('loadDropdown')\">Load</div><ul>";
    for(var i = 0; i < 10; i++) ret += "<li onclick='VMLS.Load("+i+");'>Slot " + i + "</li>"
    ret += "</ul>"

    return ret;
  };
  sdd.generator = function() {
    var rect = $("#btSave")[0].getBoundingClientRect();
    $("#saveDropdown").css('left', rect.x - parseInt($("#btSave").css('padding-left')) - 1);
    $("#saveDropdown").css('top', rect.y/* + rect.height + parseInt($("#btSave").css('padding-bottom'))*/ - parseInt($("#btSave").css('padding-top')));

    var ret = "<div onclick=\"WMKit.Hide('saveDropdown')\">Save</div><ul>";
    for(var i = 0; i < 10; i++) ret += "<li onclick='VMLS.Save("+i+");'>Slot " + i + "</li>"
    ret += "</ul>"

    return ret;
  };
});
