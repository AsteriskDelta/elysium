EntityStatIns = function() {
  this.max = 0;
  this.current = 0;
  this.type = null;

  this.percent = function() {
    return Math.round(100.0 * this.current / this.max);
  }
}

EntityEffectIns = function() {
  this.duration = 0;
  this.strength = 0;
  this.type = null;

  this.abbrev = function() {
    if(this.type == null) return "null";

    return this.type.abbr + " " + Utility.Romanize(this.strength);
  }

}

EntityStat = function() {
  this.id = '';
  this.name = '';
  this.abbr = '';
  this.color = '';
  this.description = '';
  this.consumable = 'false';
}

EntityStats = function() {
  var mi = EntityStats.All.length;
  for(var i = 0; i < mi; i++) {
    var ins = new EntityStatIns();
    ins.type = EntityStats.All[i];
    this[ins.type.id] = ins;
  }

  this.maxXP = function() {
    return Math.round(Math.pow(100.0, 1 + 0.05*(this.level - 1)) / 5.0) * 5.0;
  }
}

EntityStats.All = new Array();

EntityStats.Initialize = function() {
  $.getJSON("data/stats/stats.json","", function(data) {
    var ll = data.AllStats;
    var mi = ll.length;
    for(var i = 0; i < mi; i++) {
      $.getJSON("data/stats/"+ll[i]+".json","", function(st) {
        var stat = new EntityStat();
        $.extend(stat, st);
        EntityStats.All.push(stat);
      });
    }
  });
}

Entity = function() {
  this.id = '';
  this.name = ''
  this.stats = new EntityStats();
  this.inventory = new Inventory();
  this.effects = new Array();

  this.readoutID = "";

  this.updateReadout = function() {
    $(`#party_${this.id}_name`).text(this.name);

    $(`#party_${this.id}_lvl`).text(this.stats.level.current);
    $(`#party_${this.id}_xp`).text(this.stats.xp.current);
    $(`#party_${this.id}_xp_max`).text(this.stats.xp.max);

    $(`#party_${this.id}_hp`).html(`HP ${this.stats.hp.current} / ${this.stats.hp.max}`);
    $(`#party_${this.id}_mana`).html(`MP ${this.stats.mana.current} / ${this.stats.mana.max}`);
    $(`#party_${this.id}_lust`).html(`Lust ${this.stats.lust.current} / ${this.stats.lust.max}`);
    $(`#party_${this.id}_energy`).html(`Energy ${this.stats.energy.current} / ${this.stats.energy.max}`);

    $(`#party_${this.id}_hp_bar`).width(this.stats.hp.percent());
    $(`#party_${this.id}_mana_bar`).width(this.stats.mana.percent());
    $(`#party_${this.id}_lust_bar`).width(this.stats.lust.percent());
    $(`#party_${this.id}_energy_bar`).width(this.stats.energy.percent());

    for(var i = 0; i < this.effects.length; i++) {
      var ef = this.effects[i];
      var statusText = "";

      statusText += `<div class="partyStatus" id="party_${this.id}_status_${i}">${ef.abbrev()}</div>`;
      $(`#party_${this.id}_statuses`).append(statusText);

      WMKit.Tooltipfn($(`#party_${this.id}_status_${i}`), ef.tooltip);
    }
  }
  this.createReadout = function(parentID) {
    if(this.readoutID != "") return;

    var ctx = `<div class="partyMember partyMemberL" id="party_${this.id}">
      <div class="partyName" id="party_${this.id}_name">Name Here</div>
      <div class="partyLevel" id="party_${this.id}_level">
        Lvl <span id="party_${this.id}_lvl">2</span> <span id="party_${this.id}_exp" style="float:right;">Exp <span  id="party_${this.id}_xp">32</span> / <span  id="party_${this.id}_xp_max">100</span></span>
      </div>
      <div class="gBar"><span class="gBarOverlay"  id="party_${this.id}_hp">HP 60 / 100</span>
        <div class="inBar lrBar hp_g" style="width:60%"  id="party_${this.id}_hp_bar"></div>
      </div>
      <div class="gBar"><span class="gBarOverlay"  id="party_${this.id}_lust">Lust 20 / 100</span>
        <div class="inBar rlBar lust_g" style="width:20%;"  id="party_${this.id}_lust_bar"></div>
      </div>
      <div class="gBar"><span class="gBarOverlay" id="party_${this.id}_mana">Mana 30 / 30</span>
        <div class="inBar lrBar mana_g" style="" id="party_${this.id}_mana_bar"></div>
      </div>
      <div class="gBar"><span class="gBarOverlay" id="party_${this.id}_energy">Energy 30 / 100</span>
        <div class="inBar lrBar energy_g" style="width:30%;" id="party_${this.id}_energy_bar"></div>
      </div>

      <div class="partyStatusContainer" id="party_${this.id}_statuses">
      </div>
    </div>`;

    this.readoutID = `party_${this.id}`;
    $(`#${parentID}`).append(ctx);

    this.updateReadout();
  }
  this.destroyReadout = function() {
    if(this.readoutID == "") return;

    $(`${this.readoutID}`).remove();
    this.readoutID = "";
  }

  this.apply = function(stat, amount, type) {
    Console.log("apply " + stat + " " + amount + " " + type);
  }

  this.inflict = function(effect, duration, strength) {
    Console.log("inflict " + effect + " " + duration + " " + strength);
  }
}

Entity.All = new Object();

Entity.Initialize = function() {
  EntityStats.Initialize();
}

Entity.Find = function(id) {
  if(!(id in Entity.All)) return null;
  return Entity.All[id];
}
