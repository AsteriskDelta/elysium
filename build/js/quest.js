Quest = function() {
  this.id = "";
  this.name = "";
  this.stages = new Array();
}

Quests = function() {

};

Quests.Window = null;

$(function() {
  Quests.Window = WMKit.Register("questMenu");

  Quests.Window.exclusive = true;
  Quests.Window.forceRegen = false;

  Quests.Window.generator = function() {
    var ret = "<div onclick=\"WMKit.Hide('loadDropdown')\">Load</div><ul>";
    for(var i = 0; i < 10; i++) ret += "<li onclick='VMLS.Load("+i+");'>Slot " + i + "</li>"
    ret += "</ul>"

    return ret;
  };
});
