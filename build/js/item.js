Item = function() {
  this.id = "";
  this.name = "";
  this.description = "";
  this.type = "";

  this.value = 0;
  this.weight = 0;

  this.consumable = false;
  this.onUse = null;

  this.slot = "";
  this.equipable = false;
  this.onEquip = null;
  this.onUnequip = null;
}

Item.All = new Array();
Item.Lookup = new Object();

Item.Register = function(item) {

}

Item.ByID = function(id) {

}
