$(function() {
  $("body").append("<div id='editDropdown' class=\"dropDown topButton hidden\"></div>");

  var edd = WMKit.Register("editDropdown");

  edd.exclusive = true;
  edd.forceRegen = true;

  edd.generator = function() {
    var rect = $("#btEdit")[0].getBoundingClientRect();
    $("#editDropdown").css('left', rect.x - parseInt($("#btEdit").css('padding-left')) - 1);
    $("#editDropdown").css('top', rect.y - parseInt($("#btEdit").css('padding-top')));

    var ret = "<div onclick=\"WMKit.Hide('editDropdown')\">Edit</div><ul>";
    ret += "<li onclick=\"\">Item</li>";
    ret += "<li onclick=\"\">Entity</li>";
    ret += "<li onclick=\"\">Event</li>";
    ret += "<li onclick=\"\">Map</li>";
    ret += "<li onclick=\"\" class=\"empty\"></li>";
    ret += "<li onclick=\"\">test</li>";
    ret += "</ul>"

    return ret;
  };
});
