Reader = function() {

}

Reader.Backlog = new Array();
Reader.BacklogMax = 20;

Reader.Clear = function() {
  Reader.Backlog.clear();
  Reader.Update();
}

Reader.Update = function() {
  while(Reader.Backlog.length > Reader.BacklogMax) Reader.Backlog.shift();

  var rawHtml = "";
  var len = Reader.Backlog.length;
  for(var i = 0; i < len; i++) {
    rawHtml += Reader.Backlog[i] + "<br />";
  }
  $("#stageText").html(rawHtml);
}

Reader.Display = function(str) {
  Reader.Backlog.push(str);
  Reader.Update();
}
