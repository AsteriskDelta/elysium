Time = function() {
  this.raw = 0;
  this.day = function() {
    return Math.ceil(this.raw /  1440);
  }
  this.hour = function() {
    return Math.floor((this.raw/ 60) % 24);
  }
  this.minute = function() {
    return this.raw % 60;
  }
  this.dayName = function() {
    return Time.DayNames[(this.day() + Time.FirstDayOffset) % Time.DayNames.length];
  }

  this.pretty = function() {
    return "Day " + this.day() + ", " + this.dayName() + ", " + this.hour() + ":" + this.minute();
  }
}

Time.FirstDayOffset = 3;
Time.DayNames = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};


Game = function() {

}

Game.Time = new Time();
