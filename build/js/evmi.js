EVMI = function() {
  this.main = new Array();
  this.offset = 0;
  this.yielded = false;

  this.prepare = function() {
    /*this.length = Object.keys(this.main).length;*/
  }

  this.execute = function() {
    /*while(!this.yielded && this.offset < this.length) {
      var label = Objects.keys(this.main)[this.offset];
      var obj = this.main[label];
    }*/
    while(!this.yielded && this.offset < this.main.length) {
      var cmd = this.main[this.offset];
      VM.Exec(cmd, this);

      this.offset++;
    }
  }
};
