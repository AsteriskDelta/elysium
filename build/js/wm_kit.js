function WMKit() {

}
WMKit.Entry = function() {
  this.id = "";
  this.generator = function() { return ""; };
  this.forceRegen = false;
  this.exclusive = false;
  this.isActive = false;

  this.show = function(owner = '') {
    if(this.exclusive && WMKit.ExclusiveID != '') {
      WMKit.Hide(WMKit.ExclusiveID);
    }

    if(this.exclusive && !this.isActive) {
      WMKit.SetGhosted(true);
      WMKit.ExclusiveID = this.id;
    }

    if(this.forceRegen) {
      $("#"+this.id).html(this.generator());
    }

    if(owner != '') {
      $("#"+owner).addClass("active");
      WMKit.ExcluseOwnerID = owner;
    }

    $("#"+this.id).removeClass("hidden");
    $("#"+this.id).addClass("shown");
    this.isActive = true;
  }
  this.hide = function(owner = '') {
    if(!this.isActive) return;

    this.isActive = false;
    if(this.exclusive) {
      WMKit.SetGhosted(false);
      WMKit.ExclusiveID = "";
    }

    if(owner == '') owner = WMKit.ExcluseOwnerID;
    if(owner != '') {
      $("#"+owner).removeClass("active");
      WMKit.ExcluseOwnerID = '';
    }

    $("#"+this.id).removeClass("shown");
    $("#"+this.id).addClass("hidden");
  }
}

WMKit.Entries = new Object();
WMKit.ExclusiveID = "";
WMKit.ExcluseOwnerID = "";
WMKit.Ghosted = false;

WMKit.SetGhosted = function(state) {
  if(state) {
    $("#wmGhosting").addClass("shown");
    $("#wmGhosting").removeClass("hidden");
    //$("#container").addClass("ghosted");
  } else {
    if(WMKit.ExclusiveID != "") {
      WMKit.Hide(WMKit.ExclusiveID);
    }

    $("#wmGhosting").addClass("hidden");
    $("#wmGhosting").removeClass("shown");
    //$("#container").removeClass("ghosted");
  }
  WMKit.Ghosted = state;
}

WMKit.Show = function(id, owner = '') {
  WMKit.Entries[id].show(owner);
};
WMKit.Hide = function(id, owner = '') {
  WMKit.Entries[id].hide(owner);
};
WMKit.Toggle = function(id, owner = '') {
  if(WMKit.Entries[id].isActive) WMKit.Entries[id].hide(owner);
  else WMKit.Entries[id].show(owner);
}

WMKit.Register = function(id) {
  var ret = WMKit.Entries[id] = new WMKit.Entry();
  ret.id = id;
  ret.hide();
  return ret;
}

WMKit.Tooltip = function(obj, text) {
  var setTimeoutConst;
  obj.on('hover',
    function() {
       setTimeoutConst = setTimeout(function(){
         alert(text);
       }, 500);
  }, function(){
      clearTimeout(setTimeoutConst );
  });
}

WMKit.Tooltipfn = function(obj, fn) {
  WMKit.Tooltip(obj, fn());
}
