TileType = function() {
  this.id = "";
  this.name = "";
  this.abbrev = "";
  this.color = new Color();


}

Tile = function() {
  this.type = null;

  this.render = function(pos) {
    if(this.type == null) return "";
    /*width:${Tile.Width}; height:${Tile.Height};*/
    return `<div class="tile" style="left:${pos.x}px; bottom:${pos.y}px;  background-color:${this.type.color.css()};" onclick='Map.TileAction(${pos.x},${pos.y});'>${this.type.abbrev}</div>`;
  }
}

Tile.Width = 32;
Tile.Height = 32;
