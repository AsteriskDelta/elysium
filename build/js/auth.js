Auth = function() {

}

Auth.User = "user";
Auth.Platform = "web";
Auth.Token = "";

Auth.LoggedIn = false;

Auth.Dropdown = null;
Auth.LoginWindow = null;
Auth.RegisterWindow = null;

Auth.Login = function(identity, password) {

}

Auth.Logout = function() {
  if(!Auth.LoggedIn) return true;

}

$(function() {
  $("body").append("<div id='authDropdown' class=\"dropDown topButton hidden\"></div>");

  Auth.Dropdown = WMKit.Register("authDropdown");

  Auth.Dropdown.exclusive = true;
  Auth.Dropdown.forceRegen = true;

  Auth.Dropdown.generator = function() {
    var rect = $("#btAccount")[0].getBoundingClientRect();
    $("#authDropdown").css('left', rect.x - parseInt($("#btAccount").css('padding-left')) - 1);
    $("#authDropdown").css('top', rect.y - parseInt($("#btAccount").css('padding-top')));

    var ret = "<div onclick=\"WMKit.Hide('authDropdown')\">Account</div><ul>";

    if(Auth.LoggedIn) {
      ret += `<li onclick="">My Account</li>`;
      ret += `<li class="empty"></li><li onclick="Auth.Logout();">Logout</li>`
    } else {
      ret += `<li onclick="WMKit.Toggle('authLogin');">Login</li>`;
      ret += `<li onclick="WMKit.Toggle('authRegister');">Register</li>`;
    }

    ret += "</ul>"

    return ret;
  };

  $("body").append("<div id='authLogin' class=\"modal centered hidden\"></div>");
  $("body").append("<div id='authRegister' class=\"modal centered hidden\"></div>")

  Auth.LoginWindow = WMKit.Register("authLogin");
  Auth.LoginWindow.exclusive = true;
  Auth.LoginWindow.forceRegen = true;
  Auth.LoginWindow.generator = function() {
    var ret = "<div class='vform'><div class='formTitle'>Login</div>";
    ret += `<div class="formEntry"><span class="formHeader">Username/Email</span><input  class="formValue" type=text id="authIdentity" /></div>`;

    ret += `<div class="formEntry"><span class="formHeader">Password</span><input type=password class="formValue" id="authPassword" /></div>`;
    ret += `<div class="formRow"><button onclick="">Login</button>`;

    ret += "</div>";
    return ret;
  }

  Auth.RegisterWindow = WMKit.Register("authRegister");
  Auth.RegisterWindow.exclusive = true;
  Auth.RegisterWindow.forceRegen = true;
  Auth.RegisterWindow.generator = function() {
    var ret = "<div class='vform'><div class='formTitle'>Register</div>";
    ret += `<div class="formEntry"><span class="formHeader">Username</span><input class="formValue" type=text id="regUsername" /></div>`;

    ret += `<div class="formEntry"><span class="formHeader">Email [Optional]</span><input class="formValue" type=email id="regEmail" /></div>`;

    ret += `<div class="formEntry"><span class="formHeader">Password</span><input type=password class="formValue" id="regPassword" /></div>`;
    ret += `<div class="formEntry"><span class="formHeader">Password [Confirm]</span><input class="formValue" type=password id="regPasswordConfirm" /></div>`;

    ret += `<div class="formRow"><button onclick="">Register</button>`;

      ret += "</div>";
    return ret;
  }
});
