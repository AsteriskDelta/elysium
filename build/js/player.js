WPSEvent = function() {
  this.words = 0;
  this.seconds = 0;
}

PlayerTrait = function() {
  this.id = "";
  this.name = "";
  this.value = 0;
  this.numerator = 0;
  this.denominator = 0;
}

Player = function() {
  
}

Player.Traits = new Object();
Player.MKTrait = function(id, name) {
  var newTrait = new PlayerTrait();
  newTrait.id = id;
  newTrait.name = name;
  Player.Traits[id] = newTrait;
  return newTrait;
}

Player.Entity = null;

$(function() {
  Player.MKTrait("sub", "Submissiveness");
  Player.MKTrait("dom", "Dominance");
  Player.MKTrait("pos", "Possessiveness");
  Player.MKTrait("mon", "Monster Preference");
  Player.MKTrait("vio", "Violence Preference");

  Player.MKTrait("deaths", "Deaths");
  Player.MKTrait("losses", "Losses");
  Player.MKTrait("wins", "Wins");

  Player.MKTrait("wc", "Wordcount");
  Player.MKTrait("tc", "Timecount");

  var wps = Player.MKTrait("wps", "Words per Second");

  wps.events = Array();
  wps.stddev = 0;
  wps.recalculate = function() {

  }
});
