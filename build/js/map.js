Vec2 = function(nx = 0, ny = 0) {
  this.x = nx;
  this.y = ny;

  this.plus = function(o) {
    return new Vec2(this.x + o.x, this.y + o.y);
  }
  this.minus = function(o) {
    return new Vec2(this.x - o.x, this.y - o.y);
  }
  this.times = function(o) {
    return new Vec2(this.x * o.x, this.y * o.y);
  }
  this.mod = function(o) {
    return new Vec2(this.x % o.x, this.y % o.y);
  }
  this.divideBy = function(o) {
    return new Vec2(this.x / o.x, this.y / o.y);
  }
  this.negate = function() {
    return new Vec2(-this.x, -this.y);
  }
}

class MapPosition {
  constructor() {
    this.position = new Vec2();
    this.slice = null;
  }
  get sliceID() {
    return this.slice.id;
  }
  get sliceIdx() {
    return this.slice.index;
  }
  set sliceID(id) {
    this.slice = MapSlice.SlicesByID[id];
  }
  set sliceIdx(idx) {
    this.slice = MapSlice.Slices[idx];
  }
}

TileGroup = function() {
  this.data = new Array();
  this.width = 0;
  this.height = 0;
  this.position = new Vec2();
  this.slice = 0;

  this.size= function() {
    return new Vec2(this.width, this.height);
  }
  this.far = function() {
    return this.position.plus(this.size());
  }

  this.at = function(pos) {
    var i = pos.y * this.width + pos.x;
    //alert(`${i} / ${this.data.length}`);
    if(i >= this.data.length) return null;
    else return this.data[i];
  }

  this.contains = function(pos) {
    var p = pos.minus(this.position);
    return p.x < this.width && p.y < this.height;
  }

  this.draw = function(container) {
    var raw = "";
    for(var y = 0; y < this.height; y++) {
      for(var x = 0; x < this.width; x++) {
        var i = y * this.width + x;
        var offset = this.position;

        offset.x += x * Tile.Width;
        offset.y += y * Tile.Height;

        raw += this.data[i].render(offset);
      }
    }

    container.append(raw);
  }
}

TileGroup.DefaultSize = new Vec2(5, 5);

MapSlice = function() {
  this.id = "";
  this.name = "";
  this.index = -1;
  this.groups = new Array();
  this.min = new Vec2();
  this.max = new Vec2();
  this.groupSize = new Vec2(TileGroup.DefaultSize.x, TileGroup.DefaultSize.y);

  this.tiles = new Object();
  this.syms = new Object();

  this.addSym = function(sym, tileID) {
    this.tiles[sym] = tileID;
    this.syms[tileID] = sym;
  }

  this.getTile = function(sym) {
    if(sym == ' ') return null;
    else return this.tiles[sym];
  }

  this.at = function(pos, create = false) {
    var ret = null;
    var group = null;

    for(var i = 0; i < this.groups.length; i++) {
      var grp = this.groups[i];
      if(grp.contains(pos)) group = grp;
    }

    if(group == null && !create) return null;
    else if(group == null) {
      group = new TileGroup();
      group.width = this.groupSize.x;
      group.height = this.groupSize.y;
      group.slice = this.index;
      group.position = pos.minus(pos.mod(this.groupSize));
      //group.data.resize(group.width * group.height);
      for(var i = 0; i < group.width * group.height; i++) group.data.push(new Tile());
      //alert(`made ${group.data.length}`);


      this.groups.push(group);
    }

    ret = group.at(pos.mod(this.groupSize));

    return ret;
  }

  this.scanLine = function(topOff, line) {
    for(var i = 0; i < line.length; i++) {
      var pos = new Vec2();
      pos.x = i + this.min.x;
      pos.y = topOff + this.min.y;

      var ins = this.at(pos, true);
      ins.type = this.getTile(line[i]);
    }
  }

  this.mkKey = function() {
    var ret = new Object();
    for (var sym in this.tiles) {
      if (this.tiles.hasOwnProperty(sym)) {
        var tile = this.tiles[sym];
        ret[sym] = tile.id;
      }
    }
  }

  this.getSym = function(tileID) {
    return this.syms[tileID];
  }

  this.rasterize = function() {
    var ret = new Array();

    var sz = this.max.minus(this.min);
    ret.resize(sz.y);
    for(var yp = 0; yp < sz.y; yp++) {
      var line = "";
      for(var xp = 0; xp < sz.x; xp++) {
        var off = new Vec2(xp,yp);
        off = off.plus(this.min);
        var t = this.at(off);
        if(t.type == null) line += ' ';
        else line += this.getSym(t.type.id);
      }
      ret[sz.y - yp - 1] = line;
    }

    return ret;
  }

  this.save = function() {
    var obj = new Object();
    obj.id = this.id;
    obj.name = this.name;
    obj.key = this.mkKey();
    obj.map = this.rasterize();

    var url = `data/places/${obj.id}.json`;
    //Write the data to the server/local copy
    var data = JSON.stringify(obj);
    console.log(`save to ${url}`);
    console.log(data);
  }

  this.load = function(nid) {
    var url = `data/places/${nid}.json`;
    $.getJSON(url,"", async function(data) {
      this.id = nid;
      this.name = data.name;
      var k = data.key;

      for (var sym in k) {
        if (k.hasOwnProperty(sym)) {
          var tileID = k[sym];
          this.addSym(sym, tileID);
          if(!Map.HasTile(tileID)) Map.LoadTile(tileID);
        }
      }

      var width = 0;
      for(var line = 0; line < data.map.length; line++) width = Math.max(width, data.map[line].length);
      var height = data.map.length;
      this.max.x = width;
      this.max.y = height;
      this.min.x = 0;
      this.min.y = 0;

      this.groupSize.x = TileGroup.DefaultSize.x;
      this.groupSize.y = TileGroup.DefaultSize.y;

      for(var line = 0; line < data.map.length; line++) {
        this.scanLine(data.map.length - line - 1, data.map[line]);
      }
    }.bind(this));
  }

  this.render = function(container) {
    container.empty()
    for(i = 0; i < this.groups.length; i++) {
      this.groups[i].draw(container);
    }
  }
}

Map = function() {

}

Map.CurrentSlice = -1;
Map.LoadedSlices = 0;
Map.CurrentFocus = new Vec2();
Map.Slices = new Array();
Map.SlicesByID = new Object();//Maps slices by ID

Map.Tiles = new Object();

Map.Initialize = function() {

}

//Pan to show currentSlice, currentFocus in viewport
Map.Pan = function() {

}

Map.SetFocus = function(pos, slice = -1) {
  if(slice != -1) Map.CurrentSlice = slice;
  Map.CurrentFocus = pos;
  Map.Pan();
}

Map.Render = function() {
  return Map.Slices[Map.CurrentSlice].render($(`#${mcanvas}`));
}

Map.TileAction = function(x, y) {
  var pos = new Vec2(x,y);
  alert(`${x},${y}`);
}

Map.HasTile = function(tileID) {
  if(tileID in Map.Tiles) return true;
  else return false;
}

Map.LoadSlice = function(sliceID) {
  var slice = Map.SlicesByID[sliceID] = new MapSlice();
  Map.Slices[Map.LoadedSlices] = slice;
  slice.index = Map.LoadedSlices;
  Map.LoadedSlices++;

  slice.load(sliceID);
}

Map.LoadTile = function(tileID) {
  $.getJSON(`data/tiles/${tileID}.json`,"", function(data) {
    var tl = Map.Tiles[tileID] = new TileType();
    tl.id = tileID;
    tl.name = data.name;
    tl.abbrev = data.abbrev;
    tl.color = Color.From(data.color);
  }.bind(tileID));
}
