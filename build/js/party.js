class Party {
  constructor() {
    this.id = "";
    this.index = Party.NextID++;
    this.name = "";

    this.members = new Array();
    this.map = new MapPosition();
  }

  size() {
    return this.members.length;
  }

  register() {
    if(this.id != "") Party.PartiesById[this.id] = this;
    Party.Parties = this.index;
  }
  unregister() {
    if(this.id != "") delete Party.PartiesByID[this.id];
    delete Party.Parties[this.index];
  }

  move(dx,dy) {

  }
  leaveSlice() {
    if(this.map.slice == null) return;

    this.map.slice.removeParty(this);
    this.map.slice = null;
  }
  enterSlice(slice) {
    if(this.map.slice != null) leaveSlice();

    this.map.slice = slice;
    slice.addParty(this);
  }
  teleport(x,y,slice = "") {
    if(slice != "") {
      this.enterSlice(slice);
    }
    this.map.position = Vec2(x,y);
    this.move(0,0);
  }
}

Party.NextID = 0;
Party.Parties = new Array();
Party.PartiesByID = new Object();
