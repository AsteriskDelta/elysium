EntityEffect = function() {
  this.id = "";
  this.name = "";
  this.abbr = "";
  this.description = "";

  this.tooltip = function() {
    return `<span class="moverHeader">${this.name}</span><br /><span class="moverText">${this.description}</span>`;
  }
}
