import js.jquery.JQuery;

class Main {
  static function main() new Main();

  function new() {
    trace("DOM example");

    new JQuery(function() {
      trace("DOM ready");
      new JQuery(".container").html("<p>DOM ready</p>");
    });
  }
}
