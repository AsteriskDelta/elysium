default: Elysium
.PHONY:Elysium

Elysium: build/js/elysium.js

build/js/elysium.js: $(wildcard core/*) $(wildcard entity/*) $(wildcard map/*)
		haxe build.hxml
